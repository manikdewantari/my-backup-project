@Search
Feature: Search product by category
  user want to search product by Semua

  @SC001
  Scenario: SC001 - User search product by Semua
    Given user access to Secondhand website
    When the Homepage displayed 
    Then user click the Semua button "button_Semua"

  @SC002
  Scenario: SC002 - User search product by Hoby
    Given user access to Secondhand website
    When the Homepage displayed 
    Then user click the Hoby button "button_Hoby"

  @SC003
  Scenario: SC003 - User search product by Kendaraan
    Given user access to Secondhand website
    When the Homepage displayed 
    Then user click the Kendaraan button "button_Kendaraan"

  @SC004
  Scenario: SC004 - User search product by Elektronik
    Given user access to Secondhand website
    When the Homepage displayed 
    Then user click the Elektronik button "button_Elektronik"

  @SC005
  Scenario: SC005 - User search product by Baju
    Given user access to Secondhand website
    When the Homepage displayed 
    Then user click the Baju button "button_Baju"

  @SC006
  Scenario: SC006 - User search product by Kesehatan
    Given user access to Secondhand website
    When the Homepage displayed 
    Then user click the Baju button "button_Baju"

