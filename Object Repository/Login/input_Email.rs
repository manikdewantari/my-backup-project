<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Email</name>
   <tag></tag>
   <elementGuidId>dd7a9350-c434-40ca-a1b7-ae77618a3e96</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@class='form-control']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@class='form-control']</value>
      <webElementGuid>f70e894d-4b44-4b79-a63b-6a6c9cb25087</webElementGuid>
   </webElementProperties>
</WebElementEntity>
