<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_login</name>
   <tag></tag>
   <elementGuidId>8ab1a27c-765d-41bb-bdd1-be79fdf44448</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class='btn btn-primary w-100']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class='btn btn-primary w-100']</value>
      <webElementGuid>8ee4b021-7dbb-4efb-b933-493a7e45f4ce</webElementGuid>
   </webElementProperties>
</WebElementEntity>
