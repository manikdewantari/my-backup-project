<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Product</name>
   <tag></tag>
   <elementGuidId>280d9f1f-8418-4a85-a582-4020170af4d0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//p[.='Hyundai 3']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//p[.='Hyundai 3']</value>
      <webElementGuid>be68aba6-ec30-4c5e-a5dc-27f0c305189f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
